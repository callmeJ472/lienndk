""" Create table for sql database """
def createTable(cursor):
    print('=== Create Table')
    cursor.execute('''
        CREATE TABLE CITY (
            cityID INT NOT NULL PRIMARY KEY,
            cityName TEXT, 
            coordLat FLOAT,
            coordLon FLOAT,
            currentTimezone FLOAT, 
            previousTimezone FLOAT
        );

        CREATE TABLE SYS (
            sysID INT NOT NULL PRIMARY KEY,
            sysType INT,
            sysMessage FLOAT,
            countryCode CHAR(6),
            sunrise DATETIME,
            sunset DATETIME
        );

        CREATE TABLE WEATHER (
            weatherID INT NOT NULL PRIMARY KEY,
            weatherMain TEXT,
            description TEXT,
            icon VARCHAR(3)
        );

        CREATE TABLE FACT_TABLE (
            weatherID INT NOT NULL FOREIGN KEY REFERENCES WEATHER(weatherID),
            dt DATETIME NOT NULL,
            sysId INT FOREIGN KEY REFERENCES SYS(sysID),
            cityID INT FOREIGN KEY REFERENCES CITY(cityID),
            temp FLOAT, 
            feels_like FLOAT,
            temp_min FLOAT,
            temp_max FLOAT,
            pressure FLOAT,
            humidity FLOAT, 
            sea_level FLOAT,
            grnd_level FLOAT,
            cloudAll INT,
            rain_1h FLOAT,
            rain_3h FLOAT,
            snow_1h FLOAT,
            snow_3h FLOAT,
            windSpeed FLOAT,
            windDeg FLOAT,
            windGust FLOAT,
            visibility INT, 
            base TEXT,
            PRIMARY KEY (dt, weatherID, cityID)
        );
    ''')
