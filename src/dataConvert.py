from cmath import exp
from time import time
from typing import Type
import pandas as pd
import numpy as np
from numpy import dtype
from datetime import datetime


""" Convert Datetime"""
def transformDatetime(unixDT, timezone):
    try:    
        temp = unixDT + timezone
        dtime = datetime.utcfromtimestamp(temp).strftime('%Y-%m-%d %H:%M:%S')
        return dtime
    except TypeError as e:
        print("Error:", e)


""" Convert Timezone """
def transformTZ(timezone):
    try: 
        return timezone/3600
    except TypeError as e:
        print("TypeError:", e)


""" 
    Convert datetime and timezone columns to the right format 
"""
def transformData(df):
    ## transform Datetime
    dtFeatures = ['sys.sunrise', 'sys.sunset', 'dt']
    for i in dtFeatures:
        df[i] = transformDatetime(df[i], df['timezone'])

    ## transform Timezone
    df['timezone'] = transformTZ(df['timezone'])
    return df


""" Convert to single pandas dataframe """
def convertData(value):
    try:
        if type(value) == int:
            return value.astype(int)
        elif type(value) == float:
            return value.astype(float)
        else: 
            return value
    except TypeError as e:
        print("TypeError:", e)


def convertSingleDF(df, expectedCol):
    single_df = pd.DataFrame(columns=expectedCol)
    for i in expectedCol:
        if i in df.columns:
            single_df[i] = convertData(df[i])
        else:
            single_df[i] = None
    return single_df


""" Convert dim tables to pandas dataframe """
def convertToPandas(data, cityCol, sysCol, factCol):
    df = pd.json_normalize(data)
    df_weather = pd.json_normalize(data, record_path=['weather'])

    ## transform data
    df = transformData(df)

    ## split dims and facts in pandas
    df_city = convertSingleDF(df, cityCol)
    df_sys = convertSingleDF(df, sysCol)

    ## converting fact table
    temp  = convertSingleDF(df, factCol)
    df_fact = pd.concat([df_weather['id'].to_frame(name='weatherID'), temp], axis='columns', join='outer')

    return df, df_weather, df_city, df_sys, df_fact