from api import callAPI
from createTable import createTable
import dataConvert as cv
import query as q

import time
import pyodbc
import datetime

### define dims and fact columns
cityCol = ['id', 'name', 'coord.lon', 'coord.lat', 'timezone', 'prev_tz']
sysCol = ['sys.id', 'sys.type', 'sys.message', 'sys.country', 'sys.sunrise', 'sys.sunset']
factCol = ['dt', 'sys.id', 'id', 'main.temp', 'main.feels_like', 'main.temp_min', 'main.temp_max', 
    'main.pressure', 'main.humidity', 'main.sea_level', 'main.grnd_level', 'clouds.all', 
    'rain.1h', 'rain.3h', 'snow.1h', 'snow.3h',  'wind.speed', 'wind.deg', 'wind.gust', 'visibility', 'base']

cityList = ['London,uk', 'Hanoi', 'Canada', 'Hawaii']

connection = pyodbc.connect(
        "Driver={SQL Server};"
        "Server=CVPLIENNDK-8;"
        "Database=db_WEATHER;"
        "Trusted_Connection=yes;"
    )
connection.autocommit = True

### Tao bang
print('Have you created the table?')
print('[1] YES      [2] NO')
inp = int(input())
run = True
while(run):
    if (inp == 1):
        run = False
        pass
    elif (inp == 2):
        createTable(connection)
        run = False
    else: run = True

def main(max_timer=7200):
    timer = 0 
    while(timer <= max_timer):
        print("--- Start crawling at", datetime.datetime.now())
        connection.execute("USE db_WEATHER;")
        for i in cityList:
            ### crawl API
            print("----", i)
            data = callAPI(i)

            if data['cod'] == 200:
                ### convert crawl data
                df, df_weather, df_city, df_sys, df_fact = cv.convertToPandas(data, cityCol, sysCol, factCol)
            
                ### Insert value into Dims and Fact table
                q.insertCityDim(connection, df_city, 'CITY', 'cityID')
                q.insertDimTable(connection, df_sys, 'SYS', 'sys.id', 'sysID')
                q.insertDimTable(connection, df_weather, 'WEATHER', 'id', 'weatherID')

                q.insertFactTable(connection, df_fact, 'FACT_TABLE')
                time.sleep(60)
                timer += 60
            else:
                print(data)
                break

        time.sleep(90)
        timer += 90
        print("--- Done crawling at", datetime.datetime.now())

main(7200)
cursor = connection.cursor()
connection.execute('''
    BACKUP DATABASE db_WEATHER
    TO DISK = 'E:\\HOMEWORK\\ELT_ETL\\backup\\db_WEATHER.bak';
''')
cursor.commit()

connection.close()