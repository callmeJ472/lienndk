""""
    CREATE QUERY
"""
from datetime import datetime


def autoInsertQuery(df, df_name, pk_name, pk_value):
    values = '{}'.format(tuple(['?']*len(df.columns)))
    query = '''
        IF NOT EXISTS (SELECT * FROM {0} WHERE {2} = {3})
            BEGIN
                INSERT INTO {0} VALUES {1}
            END 
        '''.format(df_name, values.replace('\'',''), pk_name, pk_value)
    
    return query


""" exception for CITY table """
def insertCityDim(conn, df, df_name, pk_name):
    """
        explanation: 
            old_col: place of update_val (e.g: current_timezone)
            new_col: place of current_val (e.g: prev_timezone)
    """
    values = '{}'.format(tuple(['?']*len(df.columns)))
    query = '''
        IF NOT EXISTS (SELECT * FROM CITY WHERE {2} = {3})
            BEGIN
                INSERT INTO {0} VALUES {1}
            END 
        ELSE
            IF EXISTS (SELECT * FROM {0} WHERE {2} = {3} AND currentTimezone <> {4})
            BEGIN
                UPDATE {0} 
                SET previousTimezone = currentTimezone,
                    currentTimezone = {4} 
                WHERE {2} = {3};
            END
        '''.format(df_name, values.replace('\'',''), pk_name, df['id'].values[0], df['timezone'].values[0])
    cursor = conn.cursor()
    cursor.execute(query, tuple(df.values[0]))
    cursor.commit()


def queryFactTable(df, df_name, i):
    values = '{}'.format(tuple(['?']*len(df.columns)))
    query = '''
        IF NOT EXISTS (SELECT dt, weatherID FROM {2} WHERE dt = '{0}' AND weatherID = {1} AND cityID = {4})
            INSERT INTO {2} VALUES {3}
        '''.format(df['dt'].values[i], df['weatherID'].values[i], df_name, values.replace('\'',''), df['id'].values[i])
    return query


""" 
    Call query: Insert values into Dim Tables 
"""
def insertDimTable(conn, df, df_name, col_name, pk_name):
    cursor = conn.cursor() 
    for i in range(len(df)):    
        cursor.execute(autoInsertQuery(df, df_name, pk_name, df[col_name].values[i]), tuple(df.values[i]))
    conn.commit()


def insertFactTable(conn, df, df_name):
    cursor = conn.cursor()
    for i in range(len(df)):
        cursor.execute(queryFactTable(df, df_name, i), tuple(df.values[i]))
    conn.commit()