import pytest
import pandas as pd
import numpy as np
from numpy import dtype

from src.api import callAPI
import src.dataConvert as dConvert

@pytest.mark.xfail(raises=KeyError)
@pytest.mark.parametrize("city,expCod", [
    (0, '404'),
    ('Hanoi', 200),
    ('fadsfa', '404')
])
def test_API(city, expCod):
    data = callAPI(city)
    assert data['cod'] == expCod


""" 
    Test convert to local time 
"""
@pytest.mark.xfail(raises=(TypeError))
@pytest.mark.parametrize("unixDT,timezone,expectedDatetime", [
    (1653973353, -36000, "2022-05-30 19:02:33"),
    (1654026660, 7200, "2022-05-31 21:51:00"),
    ('abc', 7200, None),
    (None, 36000, None), 
    (None, None, None)
])
def test_DateConversion(unixDT, timezone, expectedDatetime):
    actualDatetime = dConvert.transformDatetime(unixDT, timezone)
    assert expectedDatetime == actualDatetime


"""
    Test timezone conversion
"""
@pytest.mark.xfail(raises=(TypeError))
@pytest.mark.parametrize("timezone,expTZ", [
    (-36000, -10.0),
    (16200, 4.5),
    (0, 0),
    ('abc', None),
    (None, None)
])
def test_TZConversion(timezone, expTZ):
    actualTZ = dConvert.transformTZ(timezone)
    assert actualTZ == expTZ

df_sample = [['dt', 1654068761], ['sys.sunrise', 1654056797], ['sys.sunset', 1654111727], ['timezone', 7200]]
expectedDF = [['dt', "2022-06-01 09:32:41"], ['sys.sunrise', "2022-06-01 06:13:17"], ['sys.sunset', "2022-06-01 21:28:47"], ['timezone', 2.0]]


"""
    Test datatype conversion
"""
@pytest.mark.xfail(raises=(TypeError, AttributeError))
@pytest.mark.parametrize("value,expDtype", [
    (np.int64(16200), 16200),
    (np.float64(17.45), 17.45),
    ('Boiz', 'Boiz')
])
def test_ConvertData(value, expDtype):
    actualDtype = dConvert.convertData(value)
    assert actualDtype == expDtype


# @pytest.mark.parametrize("inpDF, expDF", [
#     (df_sample, expectedDF)
# ])
# def test_dataTransform(inpDF, expDF):
#     actualDF = dConvert.transformData(inpDF)
#     assert actualDF == expDF
